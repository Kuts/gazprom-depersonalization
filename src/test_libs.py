from src.fields import (
    Date,
    BankCard,
    House,
    Street,
    City,
    RusRegionCode,
    Enterprise,
    EnterpriseJob,
    UserSnils,
    UserINN,
    FIO,
    UserPhone,
    RusPasportSerial,
    RusPasportIssuer,
)


def test_fields():
    my_date = '01.01.1900'
    depdate = Date()
    res = depdate.depersonalization(my_date)
    assert my_date != res
    assert len(my_date) > 0

    my_card = '4111111111111111'
    depcard = BankCard()
    assert depcard.check(my_card)
    res = depcard.depersonalization(my_card)
    assert my_card != res
    assert depcard.check(res)

    my_house = '10'
    dephouse = House()
    res = dephouse.depersonalization(my_house)
    assert my_house != res

    my_street = 'Доватора'
    depstreet = Street()
    res = depstreet.depersonalization(my_street)
    assert res != my_street

    my_city = 'Хельсинки'
    depcity = City()
    res = depcity.depersonalization(my_city)
    assert res != my_city

    my_snils = '20584200836'
    depsnils = UserSnils()
    res = depsnils.depersonalization(my_snils)
    assert depsnils.check(my_snils)
    assert depsnils.check(res)
    assert res != my_snils

    my_inn = '699520687693'    
    depinn = UserINN()
    assert depinn.check(my_inn)
    res = depinn.depersonalization(my_inn)
    assert my_inn != res
    sum12 = depinn.get_sum12(my_inn)
    assert my_inn[10:] == sum12
    sum12 = depinn.get_sum12(res)
    assert res[10:] == sum12
    assert depinn.check(res)

    