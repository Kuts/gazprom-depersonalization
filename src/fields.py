import datetime
import random
import pymorphy2
from functools import reduce, lru_cache
from faker import Faker

fake = Faker('ru_RU')
morph = pymorphy2.MorphAnalyzer()

RUS_REGION = {
    '01': 'Республика Адыгея',
    '02': 'Республика Башкортостан',
    '03': 'Республика Бурятия',
    '04': 'Республика Алтай',
    '05': 'Республика Дагестан',
    '06': 'Республика Ингушетия',
    '07': 'Кабардино-Балкарская Республика',
    '08': 'Республика Калмыкия',
    '09': 'Карачаево-Черкесская Республика',
    '10': 'Республика Карелия',
    '11': 'Республика Коми',
    '12': 'Республика Марий-Эл',
    '13': 'Республика Мордовия',
    '14': 'Республика Саха-Якутия',
    '15': 'Республика Северная Осетия-Алания',
    '16': 'Республика Татарстан',
    '17': 'Республика Тува',
    '18': 'Удмуртская Республика',
    '19': 'Республика Хакасия',
    '20': 'Чеченская Республика',
    '21': 'Чувашская Республика',
    '22': 'Алтайский край',
    '23': 'Краснодарский край',
    '24': 'Красноярский край',
    '25': 'Приморский край',
    '26': 'Ставропольский край',
    '27': 'Хабаровский край',
    '28': 'Амурская область',
    '29': 'Архангельская область',
    '30': 'Астраханская область',
    '31': 'Белгородская область',
    '32': 'Брянская область',
    '33': 'Владимирская область',
    '34': 'Волгоградская область',
    '35': 'Вологодская область',
    '36': 'Воронежская область',
    '37': 'Ивановская область',
    '38': 'Иркутская область',
    '39': 'Калининградская область',
    '40': 'Калужская область',
    '41': 'Камчатский край',
    '42': 'Кемеровская область',
    '43': 'Кировская область',
    '44': 'Костромская область',
    '45': 'Курганская область',
    '46': 'Курская область',
    '47': 'Ленинградская область',
    '48': 'Липецкая область',
    '49': 'Магаданская область',
    '50': 'Московская область',
    '51': 'Мурманская область',
    '52': 'Нижегородская область',
    '53': 'Новгородская область',
    '54': 'Новосибирская область',
    '55': 'Омская область',
    '56': 'Оренбургская область',
    '57': 'Орловская область',
    '58': 'Пензенская область',
    '59': 'Пермский край',
    '60': 'Псковская область',
    '61': 'Ростовская область',
    '62': 'Рязанская область',
    '63': 'Самарская область',
    '64': 'Саратовская область',
    '65': 'Сахалинская область',
    '66': 'Свердловская область',
    '67': 'Смоленская область',
    '68': 'Тамбовская область',
    '69': 'Тверская область',
    '70': 'Томская область',
    '71': 'Тульская область',
    '72': 'Тюменская область',
    '73': 'Ульяновская область',
    '74': 'Челябинская область',
    '75': 'Забайкальский край',
    '76': 'Ярославская область',
    '77': 'Москва',
    '78': 'Санкт-Петербург',
    '79': 'Еврейская автономная область',
    '80': 'Агинский Бурятский автономный округ',
    '81': 'Коми-Пермяцкий автономный округ',
    '82': 'Республика Крым',
    '83': 'Ненецкий автономный округ',
    '84': 'Таймырский автономный округ',
    '85': 'Усть-Ордынский Бурятский автономный округ',
    '86': 'Ханты-Мансийский автономный округ',
    '87': 'Чукотский автономный округ',
    '88': 'Эвенкийский автономный округ',
    '89': 'Ямало-Ненецкий автономный округ',
    '90': 'Московская область',
    '91': 'Калининградская область',
    '92': 'Севастополь',
    '93': 'Краснодарский край',
    '94': 'Байконур',
    '95': 'Чеченская республика',
    '96': 'Свердловская область',
    '97': 'Москва',
    '98': 'Санкт-Петербург',
    '99': 'Москва',
    '102': 'Республика Башкортостан',
    '113': 'Республика Мордовия',
    '116': 'Республика Татарстан',
    '121': 'Чувашская Республика',
    '123': 'Краснодарский край',
    '124': 'Красноярский край',
    '125': 'Приморский край',
    '126': 'Ставропольский край',
    '134': 'Волгоградская область',
    '136': 'Воронежская область',
    '138': 'Иркутская область',
    '142': 'Кемеровская область',
    '150': 'Московская область',
    '152': 'Нижегородская область',
    '154': 'Новосибирская область',
    '159': 'Пермский край',
    '161': 'Ростовская область',
    '163': 'Самарская область',
    '164': 'Саратовская область',
    '173': 'Ульяновская область',
    '174': 'Челябинская область',
    '177': 'Москва',
    '178': 'Санкт-Петербург',
    '186': 'Ханты-Мансийский автономный округ',
    '190': 'Московская область',
    '196': 'Свердловская область',
    '197': 'Москва',
    '198': 'Санкт-Петербург',
    '199': 'Москва',
    '750': 'Московская область',
    '716': 'Республика Татарстан',
    '761': 'Ростовская область',
    '763': 'Самарская область',
    '777': 'Москва',
    '799': 'Москва',
}

PASSPORT_ISSUER_PREFIX = [
    'УМВД России по ',
    'Отделом УФМС России по ',
]


class Date:
    MASK = r'(\d{1,2}\.\d{1,2}\.\d{4})'

    def __init__(self, start_date: datetime.date = None, max_days: int = None, myformat='%d.%m.%Y'):
        self.start_date = start_date
        if self.start_date is None:
            self.start_date = datetime.datetime(year=1970, month=1, day=1).date()

        self.max_days = max_days
        if self.max_days is None:
            now = datetime.datetime.now().date()
            self.max_days = (now - self.start_date).days
        self.myformat = myformat

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        new_date = self.start_date + datetime.timedelta(days=random.randrange(self.max_days))
        return new_date.strftime(self.myformat)


class BankCard:
    LOOKUP = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    MASK = r'(\b[4|5|6]\d{3}[\s-]?(\d{4}[\s-]?){2}\d{1,4}\b)|(\b\d{4}[\s-]?\d{6}[\s-]?\d{5}\b)'

    def __init__(self):
        random.seed()

    @classmethod
    def check(cls, code: str) -> bool:
        code = reduce(str.__add__, filter(str.isdigit, code))
        evens = sum(int(i) for i in code[-1::-2])
        odds = sum(cls.LOOKUP[int(i)] for i in code[-2::-2])
        return ((evens + odds) % 10 == 0)

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            code_change = fake.credit_card_number(card_type=random.choice(['visa', 'mastercard']))
            # return code_change
            current_pos = 0
            out = ''
            for char in code_in:
                if char.isdigit():
                    out += code_change[current_pos]
                    current_pos += 1
                else:
                    out += char
            return out
        return None


class House:

    def depersonalization(self, code_in: str) -> str:
        if code_in:
            return fake.building_number()
        return None


class Street:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            return fake.street_name()
        return None


class City:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            return fake.city()
        return None


class RusRegionCode:
    MASK = None

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            return str(random.choice(list(RUS_REGION.keys())))
        return None


class Enterprise:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            word = fake.company().upper()
            return f'{word}'
        return None


class EnterpriseJob:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            return fake.job()
        return None


class UserSnils:

    def __init__(self):
        random.seed()

    def __check_num_sequence(self, code: str) -> int:
        # длина максимальной последовательности одинаковых цифр
        count = 1
        max_count = 0
        prev = None
        for im in code:
            if not prev:
                prev = im
            else:
                if prev == im:
                    count += 1
                else:
                    if count > max_count:
                        max_count = count
                    count = 1
                prev = im
        return max_count

    @classmethod
    def control_number(cls, code: str) -> str:
        sum_mul = 0
        pos = 1
        for im in code[::-1]:
            sum_mul += int(im) * pos
            pos += 1
        if sum_mul < 100:
            # Если сумма меньше 100, то контрольное число равно самой сумме
            result = str(sum_mul)
            return result.zfill(2 - len(result))
        elif sum_mul in [100, 101]:
            # Если сумма равна 100 или 101, то контрольное число равно 00
            return '00'
        elif sum_mul > 101:
            # Если сумма больше 101, то сумма делится нацело на 101 и контрольное число определяется
            #   остатком от деления аналогично предыдущим двум пунктам.
            result = str(sum_mul % 101)
            return result.zfill(2 - len(result))

    @classmethod
    def check(cls, code: str) -> bool:
        main_code = code[0:9]
        control_sum = code[9:]
        generated_control_sum = cls.control_number(main_code)
        return control_sum == generated_control_sum

    def generate(self):
        code = ''.join([str(random.randint(0, 9)) for _ in range(9)])
        # если идут три цифры подряд - повторяем генерацию
        while(self.__check_num_sequence(code) >= 3):
            code = ''.join([str(random.randint(0, 9)) for _ in range(9)])
        control_number = self.control_number(code)
        return f'{code}{control_number}'

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            return self.generate()
        return None


class UserINN:
    INN10 = [2, 4, 10, 3, 5, 9, 4, 6, 8]
    INN12_N1 = [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]
    INN12_N2 = [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]

    @classmethod
    def get_sum10(cls, code: str):
        if len(code) < 9 or len(code) > 10:
            return None
        sum1 = reduce(lambda sum1_, x: sum1_ + int(x[0])*x[1], zip(code, cls.INN10), 0) % 11
        if sum1 == 10:
            return 0
        return sum1

    @classmethod
    def get_sum12(cls, code: str):

        if len(code) < 10 or len(code) > 12:
            return None

        sum1 = reduce(lambda sum1_, x: sum1_ + int(x[0])*x[1], zip(code, cls.INN12_N1), 0) % 11
        if sum1 == 10:
            sum1 = 0        
        code = f'{code}{sum1}'

        sum2 = reduce(lambda sum2_, x: sum2_ + int(x[0])*x[1], zip(code, cls.INN12_N2), 0) % 11
        if sum2 == 10:
            sum2 = 0

        return f'{sum1}{sum2}'

    @classmethod
    def check(cls, code: str):
        if len(code) == 12:
            return code[10:] == cls.get_sum12(code)
        elif len(code) == 10:
            return code[-1] == cls.get_sum10(code)
        return False
        

    def generate(self, mode='inn12'):
        if mode == 'inn12':
            region = str(random.randint(1, 92)).zfill(2)
            inspection = str(52 + random.randint(0, 48)).zfill(2)
            number = ''.join([str(random.randint(0, 9)) for _ in range(6)])
            code = f'{region}{inspection}{number}'
            control = self.get_sum12(code)
            return f'{code}{control}'
        elif mode == 'inn10':
            code = ''.join([str(random.randint(0, 9)) for _ in range(9)])
            control = self.get_sum10(code)
            return f'{code}{control}'
        else:
            raise Exception('Неизвестный код генерации')

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str, **kwargs) -> str:
        if code_in:
            return self.generate()
        return None


class FIO:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str) -> str:
        if code_in:
            if sum([morph.parse(x)[0].tag.gender == 'femn' for x in code_in.split(' ')]) > 1:
                return fake.name_female()
            return fake.name_male()
        return None


class UserPhone:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str, country='RU') -> str:
        if code_in:
            if country == 'RU':
                return f'+7{"".join([str(random.randint(0,9)) for _ in range(10)])}'
        return None


class RusPasportSerial:
    '''
    Серия и номер паспорта записываются в формате XX XX YYYYYY, где XX XX — 4-значная серия паспорта
    и YYYYYY — 6-значный номер паспорта.

    Первые две цифры серии паспорта соответствуют коду ОКАТО региона, в котором выдан паспорт; третья
    и четвёртая цифры серии паспорта соответствуют последним двум цифрам года выпуска бланка паспорта
    (допускается отклонение на 1-3 года). Пример: паспорт серии 45 04 выдан в городе Москве, а паспорт
    серии 37 11 выдан в Курганской области в 2011 году.
    '''

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str, region=None) -> str:
        if code_in:
            if region is None:
                region = random.choice(list(filter(lambda x: len(x) == 2, RUS_REGION)))
            now = datetime.datetime.now()
            year = str(random.choice([random.randint(97, 99), random.randint(0, now.year)]))
            year = year.zfill(2 - len(year))
            number = "".join([str(random.randint(0, 9)) for _ in range(6)])
            return f'{region}{year}{number}'
        return None


class RusPasportIssuer:

    @lru_cache(maxsize=128)
    def depersonalization(self, code_in: str, region=None) -> str:
        if code_in:
            region_name = random.choice(list(RUS_REGION.values()))
            if region is not None:
                region_name = RUS_REGION.get(region, region_name)

            try:
                region_name_variant = ' '.join(
                    morph.parse(x)[0].inflect({'loct'}).word for x in region_name.split(' ') if x
                )
            except Exception:
                region_name_variant = region_name
            prefix = random.choice(PASSPORT_ISSUER_PREFIX)
            return f'{prefix} {region_name_variant}'.upper()
        return None
