import csv
import datetime
import io
import random

import pandas as pd

from src.fields import (
    morph,
    Date,
    BankCard,
    House,
    Street,
    City,
    RusRegionCode,
    Enterprise,
    EnterpriseJob,
    UserSnils,
    UserINN,
    FIO,
    UserPhone,
    RusPasportSerial,
    RusPasportIssuer,
    RUS_REGION,
)


class CSVRowFaker:

    def __init__(self):
        pass

    def get_fake_fields(self, row: dict):
        return {}

    def convert(self, row: dict) -> dict:
        fake_fields = self.get_fake_fields(row)
        converted_row = {}
        for field in row:
            if field in fake_fields:
                converted_row.update({field: fake_fields[field]})
            else:
                converted_row.update({field: row.get(field)})
        return converted_row


def csv_to_fake(inputfile: str, faker_class, output_file=None):
    csvout = io.StringIO()
    filehandler = None

    with open(inputfile, encoding="utf8", newline='') as csvfile:
        reader = csv.DictReader(csvfile)

        my_reader = faker_class()
        fieldnames = next(reader)

        writer = csv.DictWriter(csvout, fieldnames=fieldnames)
        writer.writeheader()

        if output_file:
            filehandler = open(output_file, 'w', newline='', encoding='utf-8')
            filewriter = csv.DictWriter(filehandler, fieldnames=fieldnames)
            filewriter.writeheader()

        fake_data = my_reader.convert(fieldnames)
        writer.writerow(fake_data)
        if output_file:
            filewriter.writerow(fake_data)

        for row in reader:
            fake_data = my_reader.convert(row)
            writer.writerow(fake_data)
            if output_file:
                filewriter.writerow(fake_data)

    if output_file:
        filehandler.close()

    csvout.seek(0)
    df = pd.read_csv(csvout)

    return df


class AddressCSVFaker(CSVRowFaker):

    def __init__(self):
        self.region_class = RusRegionCode()
        self.street_class = Street()
        self.house_class = House()

    def get_fake_fields(self, row: dict) -> dict:
        new_region_code = self.region_class.depersonalization(str(row.get('regioncode')))
        new_region_title = RUS_REGION.get(new_region_code)
        return {
            'regionname': new_region_title,
            'regioncode': new_region_code,
            'street': self.street_class.depersonalization(
                f"{row.get('regioncode')}:{row.get('street')}" if row.get('street') else None
            ),
            'house': self.house_class.depersonalization(row.get('house')),
        }


class ApplicationCSVFaker(CSVRowFaker):

    def __init__(self):
        self.opendate_class = Date(
            start_date=datetime.datetime(2020, 1, 1).date(),
            myformat="%Y-%m-%d",
        )
        self.inputusername_class = FIO()
        self.appdatetime_class = Date(
            start_date=datetime.datetime(2020, 1, 1).date(),
            myformat="%Y-%m-%d",
        )
        self.signdate_class = Date(
            start_date=datetime.datetime(2020, 1, 1).date(),
            myformat="%Y-%m-%d",
        )
        self.card_number_class = BankCard()
        self.client_snils_class = UserSnils()
        self.client_inn_class = UserINN()

    def get_fake_fields(self, row: dict) -> dict:
        return {
            'opendate': self.opendate_class.depersonalization(row.get('opendate')),
            'inputusername': self.inputusername_class.depersonalization(row.get('inputusername')),
            'appdatetime': self.appdatetime_class.depersonalization(row.get('appdatetime')),
            'signdate': self.signdate_class.depersonalization(row.get('signdate')),
            'card_number': self.card_number_class.depersonalization(row.get('card_number')),
            'client_snils': self.client_snils_class.depersonalization(row.get('client_snils')),
            'client_inn': self.client_inn_class.depersonalization(row.get('client_inn')),
        }


class ClientCSVFaker(CSVRowFaker):

    def __init__(self):
        self.fio_class = FIO()
        self.birthplacetown_class = City()
        self.birthdate_class = Date(
            start_date=datetime.datetime(1930, 1, 1).date(),
            max_days=18000,
            myformat="%Y-%m-%d",
        )

    def get_fake_fields(self, row: dict) -> dict:
        fio = f"{row.get('lastname')} {row.get('firstname')} {row.get('middlename')}"
        fake_fio = self.fio_class.depersonalization(fio).split(' ')
        gender = 1
        if sum([morph.parse(x)[0].tag.gender == 'femn' for x in fio.split(' ')]) > 1:
            gender = 0
        return {
            'lastname': fake_fio[0],
            'firstname': fake_fio[1],
            'middlename': fake_fio[2],
            'sex': gender,
            'birthplacetown': self.birthplacetown_class.depersonalization(row.get('birthplacetown')),
            'birthdate': self.birthdate_class.depersonalization(row.get('birthdate')),
        }


class DocumentCSVFaker(CSVRowFaker):

    def __init__(self):
        self.document_id_class = UserINN()
        self.issuedate_class = Date(
            start_date=datetime.datetime(1992, 1, 1).date(),
            myformat="%Y-%m-%d",
        )
        self.passport_serial_class = RusPasportSerial()
        self.passport_isser_class = RusPasportIssuer()

    def get_fake_fields(self, row: dict):
        # получаем регион
        region = random.choice(list(filter(lambda x: len(x) == 2, RUS_REGION.keys())))
        passport_serial = self.passport_serial_class.depersonalization(row.get('series'), region=region)
        series, number = None, None

        if passport_serial:
            series = passport_serial[0:4]
            number = passport_serial[4:]

        return {
            'issue_date': lambda x: self.issuedate_class.depersonalization(row.get('issue_date')),
            'series': series,
            'number': number,
            'issuer': self.passport_isser_class.depersonalization(row.get('issuer'), region=region),
        }


class PhoneCSVFaker(CSVRowFaker):

    def __init__(self):
        self.phone_number_class = UserPhone()

    def get_fake_fields(self, row: dict):
        return {
            'phone_number': self.phone_number_class.depersonalization(row.get('phone_number')),
        }


class WorkCSVFaker(CSVRowFaker):

    def __init__(self):
        self.inn_class = UserINN()
        self.standingfrom_class = Date(start_date=datetime.datetime(2000, 1, 1).date(), myformat="%Y-%m-%d")
        self.title_class = Enterprise()
        self.positiontitle_class = EnterpriseJob()

    def get_fake_fields(self, row: dict):
        return {
            'inn': self.inn_class.depersonalization(row.get('inn')),
            'standingfrom': self.standingfrom_class.depersonalization(row.get('standingfrom')),
            'title': self.title_class.depersonalization(row.get('title')),
            'positiontitle': self.positiontitle_class.depersonalization(row.get('positiontitle')),
        }
